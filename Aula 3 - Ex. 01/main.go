package main

import "fmt"

func main() {
	for i := 0; i < 31; i++ {
		fmt.Printf("Multiplo de 3 x %d = %d \n", i, 3*i)
	}
}